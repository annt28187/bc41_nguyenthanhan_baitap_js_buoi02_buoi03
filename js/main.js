// Bai 1
function calSalary() {
  var a = document.forms["form1"]["inputNum1"].value;
  var b = document.forms["form1"]["inputNum2"].value;
  if (isNaN(a) || isNaN(b)) {
    alert("Dữ liệu không hợp lệ");
    return false;
  } else {
    var inputNum1El = document.getElementById("inputNum1").value * 1;
    var inputNum2El = document.getElementById("inputNum2").value * 1;
    var salary = inputNum1El * inputNum2El;
    document.getElementById(
      "txtSalary"
    ).innerHTML = `Tiền lương nhân viên: ${salary}`;
  }
}

// Bai 2
function averageNum() {
  var a = document.forms["form2"]["num1"].value;
  var b = document.forms["form2"]["num2"].value;
  var c = document.forms["form2"]["num3"].value;
  var d = document.forms["form2"]["num4"].value;
  var e = document.forms["form2"]["num5"].value;
  if (isNaN(a) || isNaN(b) || isNaN(c) || isNaN(d) || isNaN(e)) {
    alert("Dữ liệu không hợp lệ");
    return false;
  } else {
    var num1El = document.getElementById("num1").value * 1;
    var num2El = document.getElementById("num2").value * 1;
    var num3El = document.getElementById("num3").value * 1;
    var num4El = document.getElementById("num4").value * 1;
    var num5El = document.getElementById("num5").value * 1;
    var avgNum = (num1El + num2El + num3El + num4El + num5El) / 5;
    document.getElementById(
      "txtAverage"
    ).innerHTML = `Giá trị trung bình: ${avgNum}`;
  }
}

// Bài 3
function currencyVND() {
  var a = document.forms["form3"]["usd"].value;
  if (isNaN(a)) {
    alert("Dữ liệu không hợp lệ");
    return false;
  } else {
    const ONE_DOLLAR = 23500;
    var usdEl = document.getElementById("usd").value * 1;
    var vndEl = new Intl.NumberFormat("vn-VN").format(ONE_DOLLAR * usdEl);
    document.getElementById(
      "txtCurrency"
    ).innerHTML = `Số tiền quy đổi: ${vndEl}`;
  }
}

// Bài 4
function calRectangle() {
  var a = document.forms["form4"]["width"].value;
  var b = document.forms["form4"]["height"].value;
  if (isNaN(a) || isNaN(b)) {
    alert("Dữ liệu không hợp lệ");
    return false;
  } else {
    var widthEl = document.getElementById("width").value * 1;
    var heightEl = document.getElementById("height").value * 1;
    var perimeter = (widthEl + heightEl) * 2;
    var area = widthEl * heightEl;
    document.getElementById("txtCal").innerHTML = `\n Chu vi: ${perimeter};\n  
                    Diện tích: ${area}\n`;
  }
}

// Bài 5
function sumTwoNumber() {
  var a = document.forms["form5"]["number"].value;
  if (isNaN(a)) {
    alert("Dữ liệu không hợp lệ");
    return false;
  } else {
    var numberEl = document.getElementById("number").value * 1;
    var unit = numberEl % 10;
    var ten = Math.floor(numberEl / 10);
    var sum = ten + unit;
    document.getElementById("txtSum").innerHTML = `Tổng 2 ký số: ${sum}`;
  }
}
